@default_files = ('paper_tesis.tex');

# name of the output files, shall not contain spaces
$jobname = 'Paper-Tesis-Pablo_Velasquez';

# biber configuration
$biber = 'biber %O %S';
$bibtex_use = 1;

# set the clean up mode to clean everything but pdf, dvi or ps files
$cleanup_mode = 2;
$clean_ext = 'bbl run.xml synctex.gz';

$pdf_previewer = 'zathura';

# pdflatex options
$pdf_mode = 1;
$pdflatex = 'pdflatex -synctex=1 --interaction=nonstopmode -shell-escape %O %S';
